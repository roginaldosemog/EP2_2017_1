package ep2;

//import java.awt.Toolkit;
//import java.awt.Dimension;

public final class Game {
	
	/*
	static Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	private static final int WIDTH = ((int) screenSize.getWidth() - 190);
    private static final int HEIGHT = ((int) screenSize.getHeight() - 108);
    */

    private static final int WIDTH = 500;
    private static final int HEIGHT = 500;
    private static final int DELAY = 10;
    
    public static int getWidth(){
        return WIDTH;
    }
    
    public static int getHeight(){
        return HEIGHT;
    }
    
    public static int getDelay(){
        return DELAY;
    }
}
